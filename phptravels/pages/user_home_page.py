from library.page_object_wrapper.page_object import PageObject, HTMLElement, HTMLElementList


class UserHomePage(PageObject):

    welcome_msg = HTMLElement(xpath="//h3[@class='RTL']")

    def logout(self):
        self.webdriver.get(self.url+"/logout")