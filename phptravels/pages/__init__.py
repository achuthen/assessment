from phptravels.pages.login_page import LoginPage
from phptravels.pages.user_home_page import UserHomePage


class PhpTrabvelsPages:

    def __init__(self, driver, url):
        self.login_page = LoginPage(driver, url=url+"/login")
        self.user_home_page = UserHomePage(driver, url=url+"/account")
