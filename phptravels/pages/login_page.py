from library.page_object_wrapper.page_object import PageObject, HTMLElement, HTMLElementList


class LoginPage(PageObject):

    login_username = HTMLElement(name='username')
    login_password = HTMLElement(name='password')
    error_message = HTMLElement(xpath="//div[@class='alert alert-danger']")
    login_submit = HTMLElement(xpath="//*[@id='loginfrm']/button", expected_condition="clickable")