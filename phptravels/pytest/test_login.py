import pytest
from phptravels.pages import PhpTrabvelsPages
import csv
import logging

@pytest.fixture(scope='class', autouse=True)
def phptravels_pages(request, setup_webdriver_per_class, session_environment_variable_list):
    logging.info(session_environment_variable_list.get('URL'))
    yield PhpTrabvelsPages(setup_webdriver_per_class, session_environment_variable_list.get('URL'))

@pytest.fixture(scope='function')
def logout(request, phptravels_pages):
    yield
    phptravels_pages.user_home_page.logout()

# read test data
data_unsuccessful_login = []
with open('phptravels/pytest/test_data/test_unsuccessful_login.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    header = []
    row_number = 0

    for row in csv_reader:
        test_dict = {}
        if row_number == 0:
            for col in row:
                header.append(col)
        else:
            for idx, col in enumerate(row):
                test_dict[header[idx]] = col
            data_unsuccessful_login.append(test_dict)
        #test_dict.clear()
        row_number += 1

class TestLogin:

    @pytest.mark.usefixtures("logout")
    def test_successful_login(self, phptravels_pages):
        login_page = phptravels_pages.login_page
        login_page.navigate()
        login_page.login_username = "user@phptravels.com"
        login_page.login_password = "demouser"
        login_page.login_submit.click()
        assert phptravels_pages.user_home_page.welcome_msg.text == "Hi, Demo User"

    # @pytest.mark.parametrize("username, password",[
    #     ("wrong@mail.com", "wrong-password"),
    #     ("user@phptravels.com", "wrong-password"),
    #     ("user@phptravels.com", ""),
    #     ("", "wrong-password"),
    #     ("","")
    #
    # ])
    @pytest.mark.parametrize('test_data',data_unsuccessful_login)
    def test_unsuccessful_login(self, phptravels_pages, test_data):
        login_page = phptravels_pages.login_page
        login_page.navigate()
        login_page.login_username = test_data['username']
        login_page.login_password = test_data['password']
        login_page.login_submit.click()
        assert login_page.error_message.text == "Invalid Email or Password"

    def test_username_html_attributes(self, phptravels_pages):
        login_page = phptravels_pages.login_page
        login_page.navigate()
        assert login_page.login_username.get_attribute("type") == "email"
        assert login_page.login_username.get_attribute("placeholder") == "Email"
        assert login_page.login_username.get_attribute("required") == "true"

    def test_password_html_attributes(self, phptravels_pages):
        login_page = phptravels_pages.login_page
        login_page.navigate()
        assert login_page.login_password.get_attribute("type") == "password"
        assert login_page.login_password.get_attribute("placeholder") == "Password"
        assert login_page.login_password.get_attribute("required") == "true"

    def __test_force_fail_for_screenshot(self, phptravels_pages):
        login_page = phptravels_pages.login_page
        login_page.navigate()
        login_page.login_submit.click()
        assert False