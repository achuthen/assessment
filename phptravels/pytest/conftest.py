__author__ = 'achuthen'

import pytest
import logging
import os
import datetime
import http.client as http_client
import time
import pytest_html
from py._xmlgen import html
from selenium import webdriver
from selenium.webdriver.remote.remote_connection import LOGGER
LOGGER.setLevel(logging.WARNING)

ARTIFACT_FAIL_DIR = 'phptravels/artifacts/fail'
ARTIFACT_PASS_DIR = 'phptravels/artifacts/pass'
conftest_logger = logging.getLogger("conftest.logger")


@pytest.fixture(scope='session', autouse=True)
def session_environment_variable_list(request):
    """
    Load all the enviroment variables here.
    Call them from test cases by this fixture. Values are not real time during test. its a value when start test
    """

    env_vars = {}
    env_vars['LOG_LEVEL'] = os.environ.get("LOG_LEVEL", 'info')
    env_vars['BROWSER'] = os.environ.get('BROWSER', 'chrome')
    env_vars['REMOTE_SELENIUM'] = os.environ.get('REMOTE_SELENIUM', None)
    env_vars['REMOTE_SELENIUM_PORT'] = os.environ.get('REMOTE_PORT', "4444")
    env_vars['URL'] = os.environ.get('URL', "https://www.phptravels.net")
    return env_vars


@pytest.fixture(scope='session', autouse=True)
def setup_loggin(request):
    """
    Common Session Fixture
    1. Set log level for all the relevant logs
    2.
    """
    debug = False
    log_level = os.environ.get("LOG_LEVEL", "info")
    logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s', datefmt='%I:%M:%S')
    pg_obj_logger = logging.getLogger("page_object_logger")
    selenium_functional_logger = logging.getLogger("test.functional.logger")
    logging.basicConfig(level=logging.WARN)
    if log_level.lower() == 'debug':
        pg_obj_logger.setLevel(level=logging.DEBUG)
        selenium_functional_logger.setLevel(level=logging.DEBUG)
        conftest_logger.setLevel(level=logging.DEBUG)
        debug = True
        http_client.HTTPConnection.debuglevel = 1

        # You must initialize logging, otherwise you'll not see debug output.
        #logging.basicConfig()
        logging.getLogger().setLevel(logging.DEBUG)
        requests_log = logging.getLogger("requests.packages.urllib3")
        requests_log.setLevel(logging.DEBUG)
        requests_log.propagate = True
    else:
        pg_obj_logger.setLevel(level=logging.INFO)
        selenium_functional_logger.setLevel(level=logging.INFO)
        conftest_logger.setLevel(level=logging.INFO)



@pytest.fixture(scope='class', autouse=True)
def setup_webservice_session(request, session_environment_variable_list):
    api_request = None
    return api_request


def _remote_webdriver(browser,remote_selenium, remote_port):

    remote_url = 'http://' + remote_selenium + ':' + str(remote_port) + '/wd/hub'
    return webdriver.Remote(desired_capabilities={'browserName': browser, 'javascriptEnabled': True,
                                                    'acceptSslCerts': True},
                              command_executor=remote_url)


def _local_webdriver(browser):
    if browser == "chrome":
        return webdriver.Chrome()
    elif browser == "firefox":
        return webdriver.Firefox()


@pytest.fixture(scope='class')
def setup_webdriver_per_class(request, session_environment_variable_list):
    driver = None
    remote_selenium = session_environment_variable_list.get("REMOTE_SELENIUM")
    remote_port = session_environment_variable_list.get("REMOTE_SELENIUM_PORT")
    browser = session_environment_variable_list.get('BROWSER').lower()
    if remote_selenium:
        time.sleep(1) # gitlab runner is failing due to session. try this delay helps to fix the issue.
        driver = _remote_webdriver(browser, remote_selenium, remote_port)
    else:
        driver = _local_webdriver(browser)

    driver.implicitly_wait(5)
    driver.maximize_window()
    request.cls.driver = driver
    def tear_down_webdriver():
        conftest_logger.debug('Start setup_webdriver (conftest)')
        driver.close()
        # due to firefox bug #1443520
        try:
            driver.quit()
        except Exception as e:
            conftest_logger.debug("quit throws exception : ")
            conftest_logger.debug(e)
        conftest_logger.debug('End setup_webdriver (conftest)')
    request.addfinalizer(tear_down_webdriver)
    return driver


@pytest.fixture(scope='session', autouse=False)
def setup_db_session(request):
    db_conn = None
    return db_conn

"""=====================================================================================================================
REPORTINGS, SCREEN CAPTURES , SOME TEST FEATURES IMPLEMENTATIONS

Screen Capture
1. Selenium, or when webdriver exist the driver screen captures upon failures. Failed file is placed under failed
artifacts

Reporting

====================================================================================================================="""


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])
    if hasattr(item.cls, 'driver'):
        driver = item.cls.driver
    else:
        driver = None
    xfail = hasattr(report, 'wasxfail')
    # failure = (report.skipped and xfail) or (report.failed and not xfail)
    failure = report.failed and not xfail
    setattr(item, "rep_" + report.when, report)
    test_case_name = item.name

    # for html reporting
    report.description = str(item.function.__doc__)
    report.testname = test_case_name

    if driver is not None:
        if failure:
            try:
                suffix_name = os.environ.get('BUILD_NUMBER')
                if suffix_name is None:
                    now = datetime.datetime.now()
                    suffix_name = now.strftime("%d_%H_%M_%S")
                filename = "file_" + test_case_name + "_" + suffix_name
                conftest_logger.info('screen capturing..')
                driver.get_screenshot_as_file('%s/%s.png' % (ARTIFACT_FAIL_DIR, filename))
                extra.append(pytest_html.extras.png('%s/%s.png' % (ARTIFACT_FAIL_DIR, filename)))
                time.sleep(1)
            except Exception as e:
                conftest_logger.warning('Failed to gather screenshot: {0}'.format(e))
    report.extra = extra


#html plungin hooks
def pytest_html_results_table_header(cells):
    # cells.insert(3, html.th('Description'))
    # cells.insert(1, html.th('Time', class_='sortable time', col='time'))
    #cells.pop(1)
    cells.pop() # remove links

def pytest_html_results_table_row(report, cells):
    # cells.insert(3, html.td(report.description))
    # cells.insert(1, html.td(datetime.datetime.utcnow(), class_='col-time'))

    cells.pop() # remove links
    if hasattr(report, 'testname'):
        cells.pop(1)
        cells.insert(1, html.td(report.testname))
