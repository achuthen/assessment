from library.service_wrapper import ServiceWrapper

class TrelloBaseApi(ServiceWrapper):

    _key = None
    _token = None

    def update_key_and_token(self, key, token):
        self._key = key
        self._token = token