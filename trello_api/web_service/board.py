from ._base_trello_service import TrelloBaseApi


class TrelloBoard(TrelloBaseApi):

    def create_board(self, name):
        params = {"name": name, "key": self._key,
                       "token": self._token}
        return self._request("POST", relative_url="/boards", params=params)

    def delete_board(self, id):
        params = {"key": self._key,
                  "token": self._token}
        return self._request("DELETE", relative_url="/boards/"+id, params=params)