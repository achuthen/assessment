from ._base_trello_service import TrelloBaseApi


class TrelloList(TrelloBaseApi):

    def create_lists(self, boardId, name, idListSource=None, pos=None):
        params = {"name": name, "idBoard": boardId, "key": self._key,
                       "token": self._token}
        return self._request("POST", relative_url="/lists", params=params)