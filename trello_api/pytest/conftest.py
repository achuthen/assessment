__author__ = 'achuthen'

import pytest
import logging
import os
import datetime
import http.client as http_client
import time
import pytest_html
from py._xmlgen import html

conftest_logger = logging.getLogger("conftest.logger")

@pytest.fixture(scope='session', autouse=True)
def session_environment_variable_list(request):
    """
    Load all the enviroment variables here.
    Call them from test cases by this fixture. Values are not real time during test. its a value when start test
    """

    env_vars = {}
    env_vars['LOG_LEVEL'] = os.environ.get("LOG_LEVEL", 'info')
    env_vars['BROWSER'] = os.environ.get('BROWSER', 'chrome')
    env_vars['URL'] = os.environ.get("URL", "https://api.trello.com/1/")

    return env_vars

@pytest.fixture(scope='session', autouse=True)
def trello_api_key(request):
    # todo: read from config and return
    yield "c32d705c8a81d59a5db854ec35dbd279"

@pytest.fixture(scope='session', autouse=True)
def trello_api_token(request):
    # todo: read from config and return
    yield "d8e8d5c9feea0492af38adff8b50e9aa74ae08d63ef598d70dc345a7b52d506a"


"""=====================================================================================================================
REPORTINGS, SCREEN CAPTURES , SOME TEST FEATURES IMPLEMENTATIONS

Screen Capture
1. Selenium, or when webdriver exist the driver screen captures upon failures. Failed file is placed under failed
artifacts

Reporting

====================================================================================================================="""


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])
    if hasattr(item.cls, 'driver'):
        driver = item.cls.driver
    else:
        driver = None
    xfail = hasattr(report, 'wasxfail')
    # failure = (report.skipped and xfail) or (report.failed and not xfail)
    failure = report.failed and not xfail
    setattr(item, "rep_" + report.when, report)
    test_case_name = item.name

    # for html reporting
    report.description = str(item.function.__doc__)
    report.testname = test_case_name


#html plungin hooks
def pytest_html_results_table_header(cells):
    # cells.insert(3, html.th('Description'))
    # cells.insert(1, html.th('Time', class_='sortable time', col='time'))
    #cells.pop(1)
    cells.pop() # remove links

def pytest_html_results_table_row(report, cells):
    # cells.insert(3, html.td(report.description))
    # cells.insert(1, html.td(datetime.datetime.utcnow(), class_='col-time'))

    cells.pop() # remove links
    if hasattr(report, 'testname'):
        cells.pop(1)
        cells.insert(1, html.td(report.testname))
