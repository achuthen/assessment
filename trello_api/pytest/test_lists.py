from trello_api.web_service.lists import TrelloList
from trello_api.web_service.board import TrelloBoard
import pytest
import datetime
import logging


@pytest.fixture(scope='class')
def trello_lits_api(request, session_environment_variable_list):
    header = {
        'Content-Type': "application/json"
    }
    yield TrelloList(base_url=session_environment_variable_list.get("URL"), header=header)


@pytest.fixture(scope='class')
def setup_board(request, trello_api_key, trello_api_token):
    header = {
        'Content-Type': "application/json"
    }
    board = TrelloBoard(base_url="https://api.trello.com/1/", header=header)
    board.update_key_and_token(trello_api_key, trello_api_token)
    name = "LIST_API_AUTOMATION-" + datetime.datetime.now().strftime("%d_%H_%M_%S")
    id = board.create_board(name).body['id']
    yield id
    # delete the board.
    board.delete_board(id)


class TestListApi:

    def test_successful_create_list(self, trello_api_key, trello_api_token, trello_lits_api, setup_board):
        trello_lits_api.update_key_and_token(trello_api_key,trello_api_token)
        resp = trello_lits_api.create_lists(boardId=setup_board, name="created-automated-test")
        assert resp.status_code == 200

    def test_empty_name_field_returns_400(self,trello_api_key, trello_api_token, trello_lits_api, setup_board):
        trello_lits_api.update_key_and_token(trello_api_key,trello_api_token)
        resp = trello_lits_api.create_lists(boardId=setup_board, name=None)
        assert resp.status_code == 400
        assert resp.body == "invalid value for name"

    def test_empty_board_id_field_returns_400(self,trello_api_key, trello_api_token, trello_lits_api, setup_board):
        trello_lits_api.update_key_and_token(trello_api_key,trello_api_token)
        resp = trello_lits_api.create_lists(boardId=None, name="mandatory-field-check")
        assert resp.status_code == 400
        assert resp.body == "invalid value for idBoard"

    def test_unauthorized_key_returns_error_401(self, trello_api_token, trello_lits_api, setup_board):
        trello_lits_api.update_key_and_token("wrong_key",trello_api_token)
        resp = trello_lits_api.create_lists(boardId=setup_board, name="created-automated-test")
        assert resp.status_code == 401

    def test_unauthorized_token_returns_error_401(self,trello_api_key, trello_lits_api, setup_board):
        trello_lits_api.update_key_and_token(trello_api_key, "wrong_token")
        resp = trello_lits_api.create_lists(boardId=setup_board, name="created-automated-test")
        assert resp.status_code == 401

