import requests
from requests import api
import json
import logging
import os


class GenericResponse:

    _resp = None

    @property
    def unparsed_response(self):
        return self._resp

    @unparsed_response.setter
    def unparsed_response(self, value):
        self._resp = value

    @property
    def status_code(self):
        return self._resp.status_code

    @property
    def body(self):
        try:
            return json.loads(self._resp.text)
        except:
            return self._resp.text

    def download_body(self, filename):
        chunk_size = 1000
        with open(filename, 'wb') as dfile:
            for chunk in self.unparsed_response.iter_content(chunk_size):
                dfile.write(chunk)
        filesize = os.path.getsize(filename)
        return {'download_file': filename, 'filesize': filesize}


class ServiceWrapper:
    _base_url = None
    _header = None
    _auth_token = None
    _resp = GenericResponse()
    # _last_resp_body = None
    # _last_status_code = None
    _logger = logging.getLogger('service_wrapper')

    def __init__(self, base_url, header=None):
        self._base_url = base_url
        if header is not None:
            self._header = header

    @property
    def auth_token(self):
        return self._auth_token

    @auth_token.setter
    def auth_token(self, value):
        # updates the header Authorization
        self._auth_token = value
        self._header.update(Authorization=value)

    def _request(self, method, relative_url, header=None, **kwargs):
        """

        :param method:
        :param relative_url:
        :param kwargs: json, file, data,
        :return:.
        """
        if header is None:
            header = self._header
        url = self._base_url + relative_url
        self._logger.debug("----- REQUEST CALL ---------------------------------")
        self._logger.debug("\t Endpoint: " + url)
        self._logger.debug("\t headers: " + str(header))
        for key, value in kwargs.items():
            self._logger.debug("\t {} : {}".format(key, str(value)))
        self._logger.debug("----------------------------------------------------")
        self._resp.unparsed_response = api.request(method, url=url, headers=header, **kwargs)
        self._logger.debug("----- RESPONSE  ------------------------------------")
        self._logger.debug("\t Body: " + str(self._resp.body))
        self._logger.debug("\t Status: " + str(self._resp.status_code))
        self._logger.debug("----------------------------------------------------")
        return self._resp


