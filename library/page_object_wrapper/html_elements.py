__author__ = 'achuthen'

import logging
from library.page_object_wrapper.page_object import HTMLElement, HTMLElementList


class PresenceElement(HTMLElement):
    _expected_condition = 'presence'


class CheckableElement(HTMLElement):
    def __set__(self, instance, value):
        if value is not None:
            element = self.__get__(instance, instance.__class__)
            if (value and not element.is_selected()) or \
                    (not value and element.is_selected()):
                logging.debug("Click checkable element by %s: <%s>",
                             *self._locator)
                element.click()
                return 1


class ClickableElement(HTMLElement):
    _expected_condition = 'clickable'

class Textbox(HTMLElement):
    pass

class Checkbox(CheckableElement):
    pass

class Radio(CheckableElement):
    pass

class Button(ClickableElement):
    pass

class Link(ClickableElement):
    pass


class Select(HTMLElement):

    options = HTMLElementList(xpath='./option')
    #options = HTMLElementList(xpath='//div[contains(@class, "mat-select-panel")]')

    def __set__(self, instance, value):
        if value is not None:
            value = str(value).strip()
            element = self.__get__(instance, instance.__class__)
            if len(value) > 0:
                for index in range(self.options.get_array_len):
                    # print option.text
                    # print option.text.strip()
                    if self.options[index].text.strip() == value:
                        logging.info(
                            "Select option '%s' for element <%s>: <%s>",
                            value, self._locator[0], self._locator[1])
                        self.options[index].click()
                        return 1
                raise ValueError(
                    "Select<%s> has no option with text '%s'" %
                    (self._locator[1], value))